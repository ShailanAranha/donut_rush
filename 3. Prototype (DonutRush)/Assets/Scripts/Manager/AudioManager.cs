using System.Collections.Generic;
using UnityEngine;

namespace DonutRush.Ring
{
    public class AudioManager : MonoBehaviour
    {

        /// <summary>
        /// List of audio source
        /// </summary>
        [SerializeField]
        private List<AudioData> m_lstAudios = null;

        /// <summary>
        /// Audio source for Background
        /// </summary>
        [SerializeField]
        public AudioSource m_bg = null;

        /// <summary>
        /// Audio source for sfx
        /// </summary>
        [SerializeField]
        public AudioSource m_SFX = null;

        /// <summary>
        /// Singleton instance
        /// </summary>
        private static AudioManager s_instance = null;
        public static AudioManager Instance { get { return s_instance; } }

        /// <summary>
        /// Monobehaviour awake 
        /// </summary>
        private void Awake()
        {
            s_instance = this;
        }

        /// <summary>
        /// Monobehaviour destroy 
        /// </summary>
        private void OnDestroy()
        {
            s_instance = null;
        }

        /// <summary>
        /// Play audio via id
        /// </summary>
        /// <param name="a_strID"></param>
        public void PlayAudio(string a_strID)
        {
            for (int i = 0; i < m_lstAudios.Count; i++)
            {
                if (m_lstAudios[i].Id.Equals(a_strID))
                {
                    if (m_lstAudios[i].AudioType.Equals(EAudioSources.Bg))
                    {
                        m_bg.clip = m_lstAudios[i].AudioClip;
                        m_bg.loop = m_lstAudios[i].IsLoop;
                        m_bg.volume = m_lstAudios[i].Volume;
                        m_bg.Play();
                    }
                    else
                    {
                        m_SFX.clip = m_lstAudios[i].AudioClip;
                        m_SFX.loop = m_lstAudios[i].IsLoop;
                        m_SFX.volume = m_lstAudios[i].Volume;
                        m_SFX.Play();
                    }
                }
            }
        }

        /// <summary>
        /// Get audio length via id
        /// </summary>
        /// <param name="a_strID"></param>
        /// <returns></returns>
        public float GetAudioLength(string a_strID)
        {
            for (int i = 0; i < m_lstAudios.Count; i++)
            {
                if (m_lstAudios[i].Id.Equals(a_strID))
                {
                    return m_lstAudios[i].AudioLength;
                }
            }
            Debug.LogError("ERROR:: AudioManager: Length not found");
            return 0.0f;
        }


    }

    [System.Serializable]
    public class AudioData
    {

        /// <summary>
        /// Audio id
        /// </summary>
        [SerializeField]
        private string m_strId = null;
        public string Id { get { return m_strId; } }

        /// <summary>
        /// Audio type
        /// </summary>
        [SerializeField]
        private EAudioSources m_isaudioType;
        public EAudioSources AudioType { get { return m_isaudioType; } }

        /// <summary>
        /// Audio clip
        /// </summary>
        [SerializeField]
        private AudioClip m_clip = null;
        public AudioClip AudioClip { get { return m_clip; } }

        /// <summary>
        /// Is Audio on loop?
        /// </summary>
        [SerializeField]
        private bool m_isloop = false;
        public bool IsLoop { get { return m_isloop; } }

        /// <summary>
        /// Audio volume value
        /// </summary>
        [SerializeField, Range(0, 1)]
        private float m_fltVolume = 0.0f;
        public float Volume { get { return m_fltVolume; } }

        /// <summary>
        /// Audio clip length
        /// </summary>
        /// <value></value>
        public float AudioLength { get { return m_clip.length; } }
    }

    /// <summary>
    /// Audio source types
    /// </summary>
    public enum EAudioSources
    {
        Bg, sfx
    }
}