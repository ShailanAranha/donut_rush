﻿using System;
using DonutRush.Ring;
using UnityEngine;
using UnityEngine.UI;

namespace DonutRush.Player
{
    public class UIPlayer : MonoBehaviour
    {
        /// <summary>
        /// Rect tranfrom of the parent object
        /// </summary>
        [SerializeField]
        public RectTransform m_rectSelf = null;

        /// <summary>
        /// Image component for donut
        /// </summary>
        [SerializeField]
        private Image m_imgDonut = null;

        /// <summary>
        /// Color for donut flavours
        /// </summary>
        [SerializeField]
        private Color m_colVanilla = Color.black, m_colChocolate = Color.black, m_colStawberry = Color.black;

        /// <summary>
        /// Hold current donut flavour
        /// </summary>
        private EFlavourType m_currentDonutFlavour = EFlavourType.Vanilla;
        public EFlavourType CurrentDonutFlavour { get { return m_currentDonutFlavour; } }

        /// <summary>
        /// Counter used to create a cycle for donut color switch
        /// </summary>
        private int m_iCounter = 1;

        /// <summary>
        /// Donut rotation amount
        /// </summary>
        [SerializeField]
        private float m_fltRotationAmount = 0;

        /// <summary>
        /// Speed of the donut
        /// </summary>
        [SerializeField]
        public float m_fltSpeed = 0.0f;

        /// <summary>
        /// Event delegate for every successful pass
        /// </summary>
        public event Action OnSuccessfulPass = null;

        /// <summary>
        /// Event delegate for every Unsuccessful pass
        /// </summary>
        public event Action OnUnsuccessfulPass = null;

        /// <summary>
        /// Is game current active?
        /// </summary>
        public bool m_isGameActive = true;

        /// <summary>
        /// Donut rotate clockwise?
        /// </summary>
        private bool m_isRotateClockwise = true;

        /// <summary>
        /// Is testing?
        /// </summary>
        [SerializeField]
        private bool m_isTesting = false;

        /// <summary>
        /// Ring Handler ref
        /// </summary>
        [SerializeField]
        private UIRingHandler m_UIRingHandlerRef = null;

        /// <summary>
        /// Animation component ref
        /// </summary>
        [SerializeField]
        private Animation m_animation = null;

        /// <summary>
        /// MOnobehaviour update
        /// </summary>
        private void Update()
        {
            if (!m_isGameActive)
            {
                return;
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                ChangeFlavour((EFlavourType)(m_iCounter % 3));
                m_iCounter++;
            }

            // m_fltRotationAmount += Time.deltaTime * m_fltSpeed;
            // m_rectSelf.eulerAngles = new Vector3(0, 0, -m_fltRotationAmount);

            m_fltRotationAmount += Time.deltaTime * m_fltSpeed;

            if (m_isRotateClockwise)
                m_rectSelf.eulerAngles = new Vector3(0, 0, -m_fltRotationAmount);
            else
                m_rectSelf.eulerAngles = new Vector3(0, 0, m_fltRotationAmount);

        }

        /// <summary>
        /// Changes the flavour of the donut via assigned color type 
        /// </summary>
        /// <param name="a_EFlavourType"></param>
        public void ChangeFlavour(EFlavourType a_EFlavourType)
        {
            switch (a_EFlavourType)
            {
                case EFlavourType.Vanilla:
                    m_currentDonutFlavour = EFlavourType.Vanilla;
                    m_imgDonut.color = m_colVanilla;
                    break;

                case EFlavourType.Chocolate:
                    m_imgDonut.color = m_colChocolate;
                    m_currentDonutFlavour = EFlavourType.Chocolate;
                    break;

                case EFlavourType.Stawberry:
                    m_imgDonut.color = m_colStawberry;
                    m_currentDonutFlavour = EFlavourType.Stawberry;
                    break;


                default:
                    Debug.LogError("Error::UIPlayer:: Type noit found");
                    break;
            }

            m_currentDonutFlavour = a_EFlavourType;
        }

        /// <summary>
        /// on donut interacts with rings
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.tag.Equals(UIRingHandler.TAG_NAME))
            {
                UIRing1 l_ring = other.gameObject.GetComponent<UIRing1>();
                if (l_ring.getCurrentFlavour.Equals(CurrentDonutFlavour))
                {
                    if (m_isTesting)
                        Debug.LogError("Pass");

                    OnSuccessfulPass?.Invoke();
                }
                else
                {
                    if (m_isTesting)
                    {
                        Debug.LogError("Fail");
                        Debug.LogError("Ring::" + l_ring.getCurrentFlavour.ToString());
                        Debug.LogError("Player::" + CurrentDonutFlavour.ToString());
                    }

                    OnUnsuccessfulPass?.Invoke();
                }
            }
        }

        /// <summary>
        /// Play Move_to_normal_cycle animation
        /// </summary>
        public void Play_Move_to_normal_cycle()
        {
            m_animation.Play("Move_to_normal_cycle");
            AudioManager.Instance.PlayAudio("a_change_cycle");
        }

        /// <summary>
        /// Play Move_to_reverse_cycle animation
        /// </summary>
        public void PLay_Move_to_reverse_cycle()
        {
            m_animation.Play("Move_to_reverse_cycle");
            AudioManager.Instance.PlayAudio("a_change_cycle");
        }

        /// <summary>
        /// Event raised when donut aniomation reaches the corner of the screen
        /// </summary>
        public void Move_to_normal_cycle()
        {
            m_isRotateClockwise = true;
        }

        /// <summary>
        /// Event raised when donut aniomation reaches the corner of the screen
        /// </summary>
        public void Move_to_reverse_cycle()
        {
            m_isRotateClockwise = false;
        }

        /// <summary>
        /// Event raised when Move_to_normal_cycle is completed
        /// </summary>
        public void Move_to_normal_cycle_completed()
        {
            m_UIRingHandlerRef.InitiateNormalCycle();
            AudioManager.Instance.PlayAudio("a_main_game");
        }

        /// <summary>
        /// Event raised when Move_to_reverse_cycle is completed
        /// </summary>
        public void Move_to_reverse_cycle_completed()
        {
            m_UIRingHandlerRef.InitiateReverseCycle();
            AudioManager.Instance.PlayAudio("a_main_game");
        }
    }

    /// <summary>
    /// Types of flavours
    /// </summary>
    public enum EFlavourType : byte
    {
        Vanilla = 0, Chocolate = 1, Stawberry = 2
    }
}
