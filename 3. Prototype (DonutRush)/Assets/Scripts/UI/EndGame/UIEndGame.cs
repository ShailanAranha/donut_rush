﻿using System.Collections;
using DonutRush.Player;
using DonutRush.Ring;
using DonutRush.Score;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace DonutRush.Discount
{
    public class UIEndGame : MonoBehaviour
    {
        /// <summary>
        /// UI Discount object ref
        /// </summary>
        // [SerializeField]
        // private UIDiscount m_UIDiscountRef = null;

        /// <summary>
        /// Display component for discount
        /// </summary>
        // [SerializeField]
        // private Image m_imgDiscount = null;

        /// <summary>
        /// UI score object ref
        /// </summary>
        [SerializeField]
        private UIScore m_UIScoreRef = null;

        /// <summary>
        /// Animator ref
        /// </summary>
        [SerializeField]
        private Animator m_anim = null;
        public Animator Anim { get { return m_anim; } }

        /// <summary>
        /// UI Player object ref
        /// </summary>
        [SerializeField]
        private UIPlayer m_uiPlayerRef = null;

        /// <summary>
        /// Monobehaviour awake 
        /// </summary>
        private void Awake()
        {
            m_uiPlayerRef.OnUnsuccessfulPass -= InitiateEndGame;
            m_uiPlayerRef.OnUnsuccessfulPass += InitiateEndGame;
        }

        /// <summary>
        /// Initiate end game animation
        /// </summary>
        public void InitiateEndGame()
        {
            AudioManager.Instance.PlayAudio("a_mismatch");

            StartCoroutine(Delay());
            //  m_anim.SetTrigger("end_game");
        }

        IEnumerator Delay()
        {
            float l_fltAudioLength = AudioManager.Instance.GetAudioLength("a_mismatch");
            yield return new WaitForSeconds(l_fltAudioLength);
            m_anim.SetTrigger("end_game");
            AudioManager.Instance.PlayAudio("a_start_screen");
        }

        /// <summary>
        /// Start scoire animation
        /// </summary>
        public void startScoreAnimatin()
        {
            m_UIScoreRef.Animator.SetTrigger("ScoreAnimation");
        }

        /// <summary>
        /// Raised when next button is clicked
        /// </summary>
        public void OnClick_NextBtn()
        {
            m_anim.SetTrigger("end_game_next_button_clicked");
        }

        /// <summary>
        /// Raised when next button animation is completed
        /// </summary>
        public void OnNextBtnAnimation_completed()
        {
            m_anim.SetTrigger("discount_coupon");
            m_UIScoreRef.Animator.SetTrigger("score_fade");
        }

        /// <summary>
        /// Raised when discount show animation is completed
        /// </summary>
        public void OnDiscountShowComplete()
        {
            m_anim.SetTrigger("play_again");
        }

        #region RESTART

        /// <summary>
        /// Ring Handler ref
        /// </summary>
        [SerializeField]
        private UIRingHandler m_UIRingHandlerRef = null;

        /// <summary>
        /// MainGameAnimationComplete object ref
        /// </summary>
        [SerializeField]
        private MainGameAnimationComplete m_MainGameAnimationCompleteRef = null;

        /// <summary>
        /// Scene name
        /// </summary>
        private const string SCENE_NAME = "Restart";

        /// <summary>
        /// load restart scene
        /// </summary>
        private void RestartScene()
        {
            SceneManager.LoadScene(SCENE_NAME);
        }

        /// <summary>
        /// Restart Game
        /// </summary>
        public void OnClick_PlayAgain()
        {
            m_UIRingHandlerRef.PlayRestartFade();

            m_MainGameAnimationCompleteRef.OnRestartFadeCompleted -=RestartScene;
            m_MainGameAnimationCompleteRef.OnRestartFadeCompleted +=RestartScene;
        }

        /// <summary>
        /// Quit game
        /// </summary>
        public void Quit()
        {
            Application.Quit();
        }
        #endregion
    }
}