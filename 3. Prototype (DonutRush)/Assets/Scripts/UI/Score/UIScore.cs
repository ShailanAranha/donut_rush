﻿using DonutRush.Discount;
using DonutRush.Player;
using TMPro;
using UnityEngine;

namespace DonutRush.Score
{
    public class UIScore : MonoBehaviour
    {
        /// <summary>
        /// Score of the player
        /// </summary>
        /// <value></value>
        public int Score { get; private set; } = 0;

        /// <summary>
        /// Display component for score
        /// </summary>
        [SerializeField]
        private TMP_Text m_txtScore = null;

        /// <summary>
        /// UI PLayer object reference
        /// </summary>
        [SerializeField]
        private UIPlayer m_UIPlayer = null;

        /// <summary>
        /// Mini donut ID
        /// </summary>
        private const string MINI_DONUT_ID = "MINI DONUT";

        /// <summary>
        /// Animator ref
        /// </summary>
        [SerializeField]
        private Animator m_anim = null;
        public Animator Animator { get { return m_anim; } }

        /// <summary>
        /// End game object ref
        /// </summary>
        [SerializeField]
        private UIEndGame m_UIEndGameRef = null;

        /// <summary>
        /// Monobehaviour ref
        /// </summary>
        private void Start()
        {
            m_UIPlayer.OnSuccessfulPass -= SuccessPass;
            m_UIPlayer.OnSuccessfulPass += SuccessPass;
        }

        /// <summary>
        /// Set score to assigned value
        /// </summary>
        /// <param name="a_value"></param>
        public void SetScore(int a_value)
        {
            Score = a_value;
            //m_txtScore.text = Score + " " + MINI_DONUT;
            m_txtScore.text = Score.ToString();
        }

        /// <summary>
        /// Set score for every successful pass
        /// </summary>
        private void SuccessPass()
        {
            ++Score;
            //  m_txtScore.text = Score + " " + MINI_DONUT;
            m_txtScore.text = Score.ToString();
        }

        /// <summary>
        /// Raised when score animation is completed
        /// </summary>
        private void OnScoreAnimationComplete()
        {
            m_UIEndGameRef.Anim.SetTrigger("end_game_next_button");
        }

    }
}
