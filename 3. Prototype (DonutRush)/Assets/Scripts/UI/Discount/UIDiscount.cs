﻿using System.Collections.Generic;
using DonutRush.Player;
using DonutRush.Score;
using UnityEngine;
using UnityEngine.UI;

namespace DonutRush.Discount
{
    public class UIDiscount : MonoBehaviour
    {
        /// <summary>
        /// Collection of discount
        /// </summary>
        [SerializeField]
        private List<Discount> m_lstDiscounts = null;

        /// <summary>
        /// 
        /// </summary>
        [SerializeField]
        private Image m_imgDiscountRef = null;

        [SerializeField]
        private UIScore m_UIScoreRef = null;
        
        [SerializeField]
        private UIPlayer m_UIPlayerRef = null;

        private void Start()
        {
            m_UIPlayerRef.OnUnsuccessfulPass += () =>
            {
                 // ASSIGN DISCOUNT
                EDiscountTypes l_discountType = UnlockDiscount(m_UIScoreRef.Score);
                m_imgDiscountRef.sprite = GetDiscountSprite(l_discountType);
            };
        }

        /// <summary>
        /// Get a discount sprite based on the type of discount
        /// </summary>
        /// <param name="a_type"></param>
        /// <returns></returns>
        public Sprite GetDiscountSprite(EDiscountTypes a_type)
        {
            foreach (Discount item in m_lstDiscounts)
            {
                if (item.DiscountTypes.Equals(a_type))
                {
                    return item.Img;
                }
            }

            Debug.LogError("ERROR::UIDiscount: Discount not found!!");
            return null;
        }

        /// <summary>
        /// Unlock a discount based on the acquired score
        /// </summary>
        /// <param name="a_iScore"></param>
        /// <returns></returns>
        public EDiscountTypes UnlockDiscount(int a_iScore)
        {
            if (a_iScore >= 50)
            {
                return EDiscountTypes.DonutRush;
            }
            else if (a_iScore >= 25)
            {
                return EDiscountTypes.Jumbo;
            }
            else if (a_iScore >= 10)
            {
                return EDiscountTypes.Cream;
            }
            else
            {
                return EDiscountTypes.Welcome;
            }
        }
    }

    /// <summary>
    /// Dicount class that holds its data
    /// </summary>
    [System.Serializable]
    public class Discount
    {
        /// <summary>
        /// Types of discount
        /// </summary>
        public EDiscountTypes DiscountTypes;

        /// <summary>
        /// Display sprite for that discount
        /// </summary>
        public Sprite Img;
    }

    /// <summary>
    /// Types of discount
    /// </summary>
    public enum EDiscountTypes : byte
    {
        Welcome = 0,
        Cream = 1,
        Jumbo = 2,
        DonutRush = 3
    }
}