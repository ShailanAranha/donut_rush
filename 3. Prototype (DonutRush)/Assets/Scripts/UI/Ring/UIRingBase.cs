﻿using DonutRush.Player;
using UnityEngine;
using UnityEngine.UI;

namespace DonutRush.Ring
{
    [RequireComponent(typeof(Animation))]
    //[addComponent(typeof(Animation))]
    public abstract class UIRingBase : MonoBehaviour
    {
        /// <summary>
        /// Display components for ring 
        /// </summary>
        [SerializeField]
        private Image m_imgRing = null;

        /// <summary>
        /// Rect of the parent object
        /// </summary>
        [SerializeField]
        private RectTransform m_rectTransfrom = null;

        /// <summary>
        /// Ring flavour
        /// </summary>
        private EFlavourType m_currentFlavour;
        public EFlavourType getCurrentFlavour { get { return m_currentFlavour; } }

        /// <summary>
        /// Animation component ref
        /// </summary>
        [SerializeField]
        private Animation m_animationRef = null;

        /// <summary>
        /// Local copy of the color
        /// </summary>
        private Color m_colCacheColor = Color.black;

        /// <summary>
        /// Set properties of the ring
        /// </summary>
        /// <param name="a_colFlavour"></param>
        /// <param name="a_vec2Position"></param>
        /// <param name="a_currentFlavour"></param>
        public void SetProperties(Color a_colFlavour, Vector2 a_vec2Position, EFlavourType a_currentFlavour, bool a_isRingDisco)
        {
            m_imgRing.color = a_colFlavour;
            m_rectTransfrom.anchoredPosition = a_vec2Position;
            m_currentFlavour = a_currentFlavour;

            m_colCacheColor = a_colFlavour;

            if (a_isRingDisco)
            {
                StartDiscoAnimation();
            }
        }

        /// <summary>
        /// Start Disco Animation
        /// </summary>
        private void StartDiscoAnimation()
        {
            m_animationRef.Play();
        }

        /// <summary>
        /// Stop dsico animation
        /// </summary>
        public void StopDiscoAnimationAndAssignFlavour()
        {
            m_animationRef.Stop();
            m_imgRing.color = m_colCacheColor;
            Debug.LogError("called");
        }



#if UNITY_EDITOR

        /// <summary>
        /// Assigns reference to the components (For editor use only)
        /// </summary>
        private void Reset()
        {
            m_imgRing = gameObject.GetComponent<Image>();
            m_rectTransfrom = gameObject.GetComponent<RectTransform>();
            m_animationRef = gameObject.GetComponent<Animation>();
        }
#endif
    }
}