﻿using System.Collections;
using System.Collections.Generic;
using DonutRush.Discount;
using DonutRush.Player;
using DonutRush.SaveGameData;
using DonutRush.Score;
using UnityEngine;

namespace DonutRush.Ring
{
    public class UIRingHandler : MonoBehaviour
    {
        /// <summary>
        /// Tag name id assigned to ring components
        /// </summary>
        public const string TAG_NAME = "Ring";

        /// <summary>
        /// Collection of ring instances
        /// </summary>
        [Space, SerializeField, Header("OBJECT POOLING"), Space]
        private List<UIRing> m_lstUIRings = null;
        public IReadOnlyList<UIRing> LstUIRings { get { return m_lstUIRings; } }

        /// <summary>
        /// Ring 1 template object
        /// </summary>
        [SerializeField]
        private UIRing1 m_UIRingTemplate1 = null;

        /// <summary>
        /// Ring 2 template object
        /// </summary>
        [SerializeField]
        private UIRing2 m_UIRingTemplate2 = null;

        /// <summary>
        /// Rect of the ring 1 
        /// </summary>
        [SerializeField]
        private RectTransform m_rectRing1 = null;

        /// <summary>
        /// Rect of the ring 2
        /// </summary>
        [SerializeField]
        private RectTransform m_rectRing2 = null;

        /// <summary>
        ///  Rect of the parent object holding all rings 1
        /// </summary>
        [Space, SerializeField, Header("RING MOVEMENT"), Space]
        private RectTransform m_rectRing1Half = null;

        /// <summary>
        ///  Rect of the parent object holding all rings 2
        /// </summary>
        [SerializeField]
        private RectTransform m_rectRing2Half = null;

        /// <summary>
        /// Speed of the rings
        /// </summary>
        private float m_fltSpeed = 0.0f;

        /// <summary>
        /// Start speed of ring 
        /// </summary>
        [SerializeField]
        private float m_fltStartSpeed = 200.0f;

        /// <summary>
        /// Speed increament value
        /// </summary>
        [SerializeField]
        private float m_fltSpeedIncreaseOffset = 50.0f;

        /// <summary>
        /// Max speed of the ring
        /// </summary>
        [SerializeField]
        private float m_fltMaxSpeed = 800.0f;

        /// <summary>
        /// Ring position offset
        /// </summary>
        [SerializeField]
        private float m_fltRingOffset = 0.0f;

        /// <summary>
        /// Ring movement counter
        /// </summary>
        private float l_fltMovementCounter = 0.0f;

        [SerializeField]
        private int m_iActivateDiscoRingAfter = 0;

        /// <summary>
        /// IS ring moving?
        /// </summary>
        private bool m_isRingMoving = false;

        /// <summary>
        /// Color for donut flavours
        /// </summary>
        [Space, SerializeField, Header("FLAVOURS"), Space]
        private Color m_colVanilla = Color.black;

        /// <summary>
        /// Color for donut flavours
        /// </summary>
        [SerializeField]
        private Color m_colChocolate = Color.black, m_colStawberry = Color.black;

        /// <summary>
        /// Last assigned flavour to the ring
        /// </summary>
        private EFlavourType LastAssignedFlavour { get; set; }

        /// <summary>
        /// Total number of rings spawned in current cycle 
        /// </summary>
        /// <value></value>
        private int TotalRingSpawned { get; set; } = 0;

        /// <summary>
        /// Total number of rings spawned so far
        /// </summary>
        /// <value></value>
        private int TotalRingSpawnedInTotal { get; set; } = 0;

        /// <summary>
        /// Total number of rings covered by the player in current cycle 
        /// </summary>
        /// <value></value>
        private int TotalRingCoveredByPlayer { get; set; } = 0;

        /// <summary>
        /// Total number of rings covered by the player 
        /// </summary>
        /// <value></value>
        private int TotalRingCoveredByPlayerInTotal { get; set; } = 0;

        /// <summary>
        /// Max number of rings to spawn in one cycle before initiate the reverse cycle and vise versa
        /// </summary>
        [SerializeField]
        private int m_iMaxRingInOneCycle = 0;

        /// <summary>
        /// LAst spawned position of the player
        /// </summary>
        private Vector2 l_vec2LastSpawnedRingPos = Vector2.zero;

        /// <summary>
        /// Last randomised value used to assign the colour of the ring
        /// </summary>
        private int l_lastRandomFlavour = -1;

        /// <summary>
        /// Second last randomised value used to assign the colour of the ring
        /// </summary>
        private int l_2ndlastRandomFlavour = -1;

        /// <summary>
        /// UI Player object ref
        /// </summary>
        [SerializeField]
        private UIPlayer m_UIPlayer = null;

        /// <summary>
        /// UI Start screen object ref
        /// </summary>
        [SerializeField]
        private UIStartScreen m_UIStartScreenRef = null;

        /// <summary>
        /// Animator ref
        /// </summary>
        [SerializeField]
        private Animator m_anim = null;

        /// <summary>
        /// MainGameAnimationComplete object ref
        /// </summary>
        [SerializeField]
        private MainGameAnimationComplete m_MainGameAnimationCompleteRef = null;

        /// <summary>
        /// Game object ref of base and scre component
        /// </summary>
        [SerializeField]
        private GameObject m_goBase = null, m_goScore = null;

        /// <summary>
        /// UI scre object ref
        /// </summary>
        [SerializeField]
        private UIScore m_UIScore = null;

        /// <summary>
        ///  Disco ring instance
        /// </summary>
        private UIRing l_objDiscoRing = null;

        /// <summary>
        /// Disco Ring Instance 
        /// </summary>
        /// <value></value>
        public int DiscoRingIndex { get; private set; } = -1;

        /// <summary>
        /// Is Disco Ring Assigned?
        /// </summary>
        /// <value></value>
        public bool IsDiscoRingAssigned { get; private set; } = false;

        /// <summary>
        /// Is Cycle Reverse?
        /// </summary>
        /// <value></value>
        public bool IsCycleReverse { get; private set; } = false;

        #region OBJECT POOLING 

        /// <summary>
        /// Give an object from the pool
        /// </summary>
        /// <returns></returns>
        private UIRing getPooledObject()
        {
            int l_iCount = m_lstUIRings.Count; // caching
            for (int i = 0; i < l_iCount; i++)
            {
                if (!m_lstUIRings[i].IsActive())
                {
                    return m_lstUIRings[i];
                }
            }

            UIRing1 l_obj1 = Instantiate(m_UIRingTemplate1, m_rectRing1);
            UIRing2 l_obj2 = Instantiate(m_UIRingTemplate2, m_rectRing2);

            UIRing l_obj = new UIRing(l_obj1, l_obj2);
            m_lstUIRings.Add(l_obj);
            return l_obj;
        }

        #endregion

        /// <summary>
        /// Monobehaviour awake
        /// </summary>
        private void Awake()
        {
            //m_UIPlayer.OnSuccessfulPass += () => { };

            m_UIPlayer.OnSuccessfulPass -= SuccessfullPass;
            m_UIPlayer.OnSuccessfulPass += SuccessfullPass;

            m_UIPlayer.OnUnsuccessfulPass -= UnsuccessfullPass;
            m_UIPlayer.OnUnsuccessfulPass += UnsuccessfullPass;

            m_fltSpeed = m_fltStartSpeed;

            m_UIStartScreenRef.OnAnimationComplete -= StartMainGameAniamtion;
            m_UIStartScreenRef.OnAnimationComplete += StartMainGameAniamtion;

            m_MainGameAnimationCompleteRef.OnMainGameAnimationComplete -= StartGame;
            m_MainGameAnimationCompleteRef.OnMainGameAnimationComplete += StartGame;

            m_goScore.SetActive(false);
        }

        /// <summary>
        /// Starts main game animations
        /// </summary>
        public void StartMainGameAniamtion()
        {
            m_anim.SetTrigger("mina_game_start");
        }

        /// <summary>
        /// Start main game
        /// </summary>
        public void StartGame()
        {
            m_UIScore.Animator.enabled = true;
            m_goBase.SetActive(true);
            m_UIPlayer.gameObject.SetActive(true);
            SaveData.Instnace.SetVisibilityHIghScore(true);
            //  m_goScore.SetActive(true);
            //  StartCoroutine(Delay());

            m_isRingMoving = true;


            for (int i = 0; i < 5; i++)
            {
                SpawnRing();
            }

            AudioManager.Instance.PlayAudio("a_main_game");
        }

        /// <summary>
        /// Monobehaviour update
        /// </summary>
        private void Update()
        {
            if (!m_isRingMoving)
            {
                return;
            }


            if (Input.GetKeyDown(KeyCode.P))
            {
                Time.timeScale = 0;
            }

            if (Input.GetKeyDown(KeyCode.U))
            {
                Time.timeScale = 1;
            }

            /*
            l_fltMovementCounter -= Time.deltaTime * m_fltSpeed;
            m_rectRing1Half.anchoredPosition = new Vector2(l_fltMovementCounter, m_rectRing1Half.anchoredPosition.y);
            m_rectRing2Half.anchoredPosition = new Vector2(l_fltMovementCounter, m_rectRing2Half.anchoredPosition.y);

            */
            // FOR REVERSE CYCLE
            if (IsCycleReverse)
            {
                l_fltMovementCounter += Time.deltaTime * m_fltSpeed;
                m_rectRing1Half.anchoredPosition = new Vector2(l_fltMovementCounter, m_rectRing1Half.anchoredPosition.y);
                m_rectRing2Half.anchoredPosition = new Vector2(l_fltMovementCounter, m_rectRing2Half.anchoredPosition.y);

            }
            else
            {
                l_fltMovementCounter -= Time.deltaTime * m_fltSpeed;
                m_rectRing1Half.anchoredPosition = new Vector2(l_fltMovementCounter, m_rectRing1Half.anchoredPosition.y);
                m_rectRing2Half.anchoredPosition = new Vector2(l_fltMovementCounter, m_rectRing2Half.anchoredPosition.y);

            }

        }

        /// <summary>
        /// Delay execution (NOT USED)
        /// </summary>
        IEnumerator Delay()
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            m_goScore.SetActive(true);
        }

        /// <summary>
        /// Get randomised color
        /// </summary>
        /// <returns></returns>
        private Color getRandomFlavour()
        {
            int l_iRandomNum = getManagedRandomFlavour();


            switch ((EFlavourType)l_iRandomNum)
            {
                case EFlavourType.Vanilla:
                    LastAssignedFlavour = EFlavourType.Vanilla;
                    return m_colVanilla;

                case EFlavourType.Stawberry:
                    LastAssignedFlavour = EFlavourType.Stawberry;
                    return m_colStawberry;

                case EFlavourType.Chocolate:
                    LastAssignedFlavour = EFlavourType.Chocolate;
                    return m_colChocolate;

                default:

                    Debug.LogError("ERROR::UIRINGHANDLER:Type not found");
                    return Color.black;
            }
        }

        /// <summary>
        /// Get managed random flavour (1 flavour is not repeated more than twice)
        /// </summary>
        /// <returns></returns>
        private int getManagedRandomFlavour()
        {
            int l_iRandomNum = -1;

            do
            {
                l_iRandomNum = Random.Range(0, 3);

                if (l_2ndlastRandomFlavour == -1 || l_lastRandomFlavour == -1)
                {
                    break;
                }

            } while (l_iRandomNum == l_2ndlastRandomFlavour && l_iRandomNum == l_2ndlastRandomFlavour);

            l_2ndlastRandomFlavour = l_lastRandomFlavour;
            l_lastRandomFlavour = l_iRandomNum;

            return l_iRandomNum;
        }

        /// <summary>
        /// Spawn ring
        /// </summary>
        private void SpawnRing()
        {
            UIRing l_newRing = getPooledObject();
            Vector2 l_vec2NewPosition = new Vector2(l_vec2LastSpawnedRingPos.x + m_fltRingOffset, l_vec2LastSpawnedRingPos.y);
            l_vec2LastSpawnedRingPos = l_vec2NewPosition;
            Color l_color = getRandomFlavour();

            bool l_isDiscoRIngAssigned = AssignDiscoRing();

            if (l_isDiscoRIngAssigned)
            {
                l_objDiscoRing = l_newRing;
                IsDiscoRingAssigned = true;
                Debug.LogError("Disco Ring Assigned");
            }

            l_newRing.SetProperties(l_color, l_vec2NewPosition, LastAssignedFlavour, l_isDiscoRIngAssigned);
            l_newRing.SetActive(true);

            ++TotalRingSpawned;
            ++TotalRingSpawnedInTotal;
        }

        /// <summary>
        /// Spawn rings in reverse order
        /// </summary>
        private void SpawnRingInReverse()
        {
            UIRing l_newRing = getPooledObject();
            Vector2 l_vec2NewPosition = new Vector2(l_vec2LastSpawnedRingPos.x - m_fltRingOffset, l_vec2LastSpawnedRingPos.y);
            l_vec2LastSpawnedRingPos = l_vec2NewPosition;
            Color l_color = getRandomFlavour();

            bool l_isDiscoRIngAssigned = AssignDiscoRing();

            if (l_isDiscoRIngAssigned)
            {
                l_objDiscoRing = l_newRing;
                IsDiscoRingAssigned = true;
                Debug.LogError("Disco Ring Assigned");
            }

            l_newRing.SetProperties(l_color, l_vec2NewPosition, LastAssignedFlavour, l_isDiscoRIngAssigned);
            l_newRing.SetActive(true);
            ++TotalRingSpawned;
        }

        /// <summary>
        /// Rasied on successfull pass
        /// </summary>
        private void SuccessfullPass()
        {
            ++TotalRingCoveredByPlayer;
            ++TotalRingCoveredByPlayerInTotal;

            if (m_fltSpeed < m_fltMaxSpeed)
            {
                //int l_iSpeedConstant = TotalRingCoveredByPlayer / 10;
                int l_iSpeedConstant = TotalRingCoveredByPlayerInTotal / 10;
                m_fltSpeed = m_fltStartSpeed + (++l_iSpeedConstant) * m_fltSpeedIncreaseOffset;
                m_UIPlayer.m_fltSpeed = m_fltSpeed;
            }

            if (TotalRingSpawned < m_iMaxRingInOneCycle)
            {
                if (IsCycleReverse)
                {
                    SpawnRingInReverse();
                }
                else
                {
                    SpawnRing();
                }

            }

            //if (TotalRingCoveredByPlayer > m_iMaxRingInOneCycle)
            if (TotalRingCoveredByPlayer == m_iMaxRingInOneCycle)
            {
                //   IsCycleReverse = !IsCycleReverse;
                //  SpawnRing();

                //InitiateReverseCycle();

                if (IsCycleReverse)
                {
                    m_UIPlayer.Play_Move_to_normal_cycle();
                }
                else
                {
                    m_UIPlayer.PLay_Move_to_reverse_cycle();
                }
            }
            else
            {
                // set reverse cycle
                // SpawnRingInReverse();
            }

            // if (IsCycleReverse)
            // {
            //     SpawnRingInReverse();
            // }
            // else
            // {
            //     SpawnRing();
            // }

            RevealDiscoRing();

            // ++TotalRingCoveredByPlayer;
            // ++TotalRingCoveredByPlayerInTotal;

            AudioManager.Instance.PlayAudio("a_pass");

        }

        /// <summary>
        /// Rasied on unsuccessfull pass
        /// </summary>
        private void UnsuccessfullPass()
        {
            m_isRingMoving = false;
            m_UIPlayer.m_isGameActive = false;
        }

        #region REVERSE CYCLE

        /// <summary>
        /// Changes properties to reserve cycle
        /// </summary>
        public void InitiateReverseCycle()
        {
            // DISABLE THE POOL

            foreach (UIRing l_ring in m_lstUIRings)
            {
                l_ring.SetActive(false);
            }

            // RESET DATA
            TotalRingSpawned = 0;
            TotalRingCoveredByPlayer = 0;
            DiscoRingIndex = -1;
            l_objDiscoRing = null;
            IsDiscoRingAssigned = false;
            //   m_isRingMoving = false; 
            m_UIPlayer.m_rectSelf.anchoredPosition = new Vector2(503, m_UIPlayer.m_rectSelf.anchoredPosition.y);
            //m_UIPlayer.m_rectSelf.anchoredPosition = new Vector2(-m_UIPlayer.m_rectSelf.anchoredPosition.x, m_UIPlayer.m_rectSelf.anchoredPosition.y);
            // l_vec2LastSpawnedRingPos = Vector2.zero;
            IsCycleReverse = true;

            Vector2 l_vec2NewPosition = new Vector2(l_vec2LastSpawnedRingPos.x + m_fltRingOffset + m_fltRingOffset, l_vec2LastSpawnedRingPos.y);
            l_vec2LastSpawnedRingPos = l_vec2NewPosition;


            for (int i = 0; i < 5; i++)
            {
                SpawnRingInReverse();
            }
        }

        /// <summary>
        /// Changes properties to normal cycle
        /// </summary>
        public void InitiateNormalCycle()
        {
            // DISABLE THE POOL

            foreach (UIRing l_ring in m_lstUIRings)
            {
                l_ring.SetActive(false);
            }

            // RESET DATA
            TotalRingSpawned = 0;
            TotalRingCoveredByPlayer = 0;
            DiscoRingIndex = -1;
            l_objDiscoRing = null;
            IsDiscoRingAssigned = false;
            //   m_isRingMoving = false;
            m_UIPlayer.m_rectSelf.anchoredPosition = new Vector2(-503, m_UIPlayer.m_rectSelf.anchoredPosition.y);
            // m_UIPlayer.m_rectSelf.anchoredPosition = new Vector2(-m_UIPlayer.m_rectSelf.anchoredPosition.x, m_UIPlayer.m_rectSelf.anchoredPosition.y);
            // l_vec2LastSpawnedRingPos = Vector2.zero;
            IsCycleReverse = false;

            Vector2 l_vec2NewPosition = new Vector2(l_vec2LastSpawnedRingPos.x - m_fltRingOffset - m_fltRingOffset, l_vec2LastSpawnedRingPos.y);
            l_vec2LastSpawnedRingPos = l_vec2NewPosition;


            for (int i = 0; i < 5; i++)
            {
                SpawnRing();
            }

        }

        #endregion

        #region DISCO RINGS

        /// <summary>
        /// Decides whether the current spawn ring should be a disco ring or not 
        /// </summary>
        /// <returns></returns>
        private bool AssignDiscoRing()
        {
            if (TotalRingSpawnedInTotal < m_iActivateDiscoRingAfter || TotalRingSpawned == 0 || IsDiscoRingAssigned)
            {
                return false;
            }

            int l_iRandom = Random.Range(0, 5);

            if (l_iRandom == 0)
            {
                DiscoRingIndex = TotalRingSpawned;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Reveals the flavour of disco ring
        /// </summary>
        private void RevealDiscoRing()
        {
            if (!IsDiscoRingAssigned)
            {
                return;
            }

            //  if (TotalRingCoveredByPlayer == DiscoRingIndex - 1)
            if (TotalRingCoveredByPlayer == DiscoRingIndex)
            {
                l_objDiscoRing.StopDiscoAnimationAndAssignFlavour();

                IsDiscoRingAssigned = false;
                DiscoRingIndex = -1;
                l_objDiscoRing = null;
            }
        }

        #endregion

        #region RESTART

        /// <summary>
        /// Triggers restart fade animation
        /// </summary>
        public void PlayRestartFade()
        {
            m_anim.SetTrigger("restart_fade");
        }

        #endregion

    }

    /// <summary>
    /// Main ring class that manages two half rings
    /// </summary>
    [System.Serializable]
    public class UIRing
    {
        /// <summary>
        /// Ring 1 reference
        /// </summary>
        [SerializeField]
        private UIRing1 m_ring1 = null;
        public UIRing1 Ring1 { get { return m_ring1; } }

        /// <summary>
        /// Ring 2 reference
        /// </summary>
        [SerializeField]
        private UIRing2 m_ring2 = null;
        public UIRing2 Ring2 { get { return m_ring2; } }

        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="a_ring1"></param>
        /// <param name="a_ring2"></param>
        public UIRing(UIRing1 a_ring1, UIRing2 a_ring2)
        {
            m_ring1 = a_ring1;
            m_ring2 = a_ring2;
        }

        /// <summary>
        /// Load data on both rings
        /// </summary>
        /// <param name="a_colFlavour"></param>
        /// <param name="a_vec2Position"></param>
        /// <param name="a_currentFlavour"></param>
        public void SetProperties(Color a_colFlavour, Vector2 a_vec2Position, EFlavourType a_currentFlavour, bool a_isRingDisco)
        {
            m_ring1.SetProperties(a_colFlavour, a_vec2Position, a_currentFlavour, a_isRingDisco);
            m_ring2.SetProperties(a_colFlavour, a_vec2Position, a_currentFlavour, a_isRingDisco);
        }

        /// <summary>
        /// Stops Disco Animation for both the rings
        /// </summary>
        public void StopDiscoAnimationAndAssignFlavour()
        {
            m_ring1.StopDiscoAnimationAndAssignFlavour();
            m_ring2.StopDiscoAnimationAndAssignFlavour();
        }

        /// <summary>
        /// Activate both rings
        /// </summary>
        /// <param name="a_value"></param>
        public void SetActive(bool a_value)
        {
            m_ring1.gameObject.SetActive(a_value);
            m_ring2.gameObject.SetActive(a_value);
        }

        /// <summary>
        /// Are both rings active?
        /// </summary>
        /// <returns></returns>
        public bool IsActive()
        {
            return m_ring1.gameObject.activeInHierarchy || m_ring2.gameObject.activeInHierarchy;
        }
    }
}
