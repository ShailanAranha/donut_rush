using DonutRush.Player;
using DonutRush.Restart;
using DonutRush.Score;
using TMPro;
using UnityEngine;

namespace DonutRush.SaveGameData
{
    public class SaveData : MonoBehaviour
    {

        /// <summary>
        /// Display component for high score
        /// </summary>
        [SerializeField]
        private TMP_Text m_txtHightScore = null;

        /// <summary>
        /// Score object ref
        /// </summary>
        [SerializeField]
        private UIScore m_UIScoreRef = null;

        /// <summary>
        /// UI PLayer object reference
        /// </summary>
        [SerializeField]
        private UIPlayer m_UIPlayer = null;

        /// <summary>
        /// HIgh score of the game
        /// </summary>
        /// <value></value>
        public int HighScore { get; set; } = 0;

        /// <summary>
        /// Singleton pattern
        /// </summary>
        private static SaveData s_instance = null;
        public static SaveData Instnace { get { return s_instance; } }

        /// <summary>
        /// Monobehaviour Awake
        /// </summary>
        private void Awake()
        {
            s_instance = this;
        }

        /// <summary>
        /// Monobehaviour Start
        /// </summary>
        void Start()
        {
            m_UIPlayer.OnUnsuccessfulPass -= SaveHIghtScore;
            m_UIPlayer.OnUnsuccessfulPass += SaveHIghtScore;
            AddHighScore();
        }

        /// <summary>
        /// Monobehaviour Destroy
        /// </summary>
        private void OnDestroy()
        {
            // SaveHIghtScore();
            s_instance = null;
        }

        /// <summary>
        /// High score primary key
        /// </summary>
        private const string HIGH_SCORE_KEY = "high_score";

        /// <summary>
        /// Get saved high score and add to the component
        /// </summary>
        private void AddHighScore()
        {
            try
            {
                if (PlayerPrefs.HasKey(HIGH_SCORE_KEY))
                {
                    int l_highScore = PlayerPrefs.GetInt(HIGH_SCORE_KEY);
                    HighScore = l_highScore;

                }

                m_txtHightScore.text = "HIGH SCORE:" + HighScore;

            }
            catch (System.Exception)
            {
                m_txtHightScore.text = "HIGH SCORE: 0";

            }
        }

        /// <summary>
        /// Save high score
        /// </summary>
        public void SaveHIghtScore()
        {
            if (m_UIScoreRef.Score > HighScore)
            {
                HighScore = m_UIScoreRef.Score;
                PlayerPrefs.SetInt(HIGH_SCORE_KEY, HighScore);
                m_txtHightScore.text = "HIGH SCORE:" + HighScore;
            }
        }

        /// <summary>
        /// Set visibility of display component of hight score
        /// </summary>
        /// <param name="a_value"></param>
        public void SetVisibilityHIghScore(bool a_value)
        {
            m_txtHightScore.enabled = a_value;
        }

    }

}
