using UnityEngine;
using UnityEngine.SceneManagement;

namespace DonutRush.Restart
{
    public class Restart : MonoBehaviour
    {
        /// <summary>
        /// Scene name
        /// </summary>
        private const string SCENE_NAME  = "MainGame";

        /// <summary>
        /// Monobehaviour start
        /// </summary>
        void Start()
        {
            SceneManager.LoadScene(SCENE_NAME);
        }
     
    }
}