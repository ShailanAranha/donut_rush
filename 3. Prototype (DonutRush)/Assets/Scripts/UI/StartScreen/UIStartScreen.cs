﻿using System;
using DonutRush.Ring;
using UnityEngine;

namespace DonutRush.Discount
{
    public class UIStartScreen : MonoBehaviour
    {
        /// <summary>
        /// Animator ref
        /// </summary>
        [SerializeField]
        private Animator m_anim = null;

        /// <summary>
        /// Gameobject reference of UI Compoenents
        /// </summary>
        [SerializeField]
        private GameObject m_goTitle = null, m_goButtons = null, m_goBackBtn = null;

        /// <summary>
        /// Event delegate for animation complete
        /// </summary>
        public event Action OnAnimationComplete = null;

        /// <summary>
        /// Is start game animation triggered? 
        /// </summary>
        private bool m_is_post_start_game_Played = false;

        /// <summary>
        /// Monobehaviour start
        /// </summary>
        private void Start()
        {
            AudioManager.Instance.PlayAudio("a_start_screen");
        }

        /// <summary>
        /// Raised when play button is clicked
        /// </summary>
        public void OnClick_PlayBtn()
        {
            if (!m_is_post_start_game_Played)
            {
                m_anim.SetTrigger("post_start_game");
                m_is_post_start_game_Played = true;
            }
        }

        /// <summary>
        /// Raised ehrn control button is button
        /// </summary>
        public void OnClick_ControlBtn()
        {
            m_goButtons.SetActive(false);
            m_goTitle.SetActive(false);
            m_goBackBtn.SetActive(true);
        }

        /// <summary>
        /// Raised whn back button is clicked
        /// </summary>
        public void OnClick_BackBtn()
        {
            m_goButtons.SetActive(true);
            m_goTitle.SetActive(true);
            m_goBackBtn.SetActive(false);
        }

        /// <summary>
        /// Raised when exit button is clicked
        /// </summary>
        public void OnClikc_Exit()
        {
            Application.Quit();
        }

        /// <summary>
        /// Raised when start animation is completed
        /// </summary>
        public void AnimationCompleted()
        {
            OnAnimationComplete?.Invoke();
        }
    }
}