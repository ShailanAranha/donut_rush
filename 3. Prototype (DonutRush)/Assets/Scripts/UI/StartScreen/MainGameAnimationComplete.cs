﻿using System;
using UnityEngine;

namespace DonutRush.Discount
{
    public class MainGameAnimationComplete : MonoBehaviour
    {
        /// <summary>
        /// Event delegate for main game animarion complete
        /// </summary>
        public event Action OnMainGameAnimationComplete = null;

        /// <summary>
        ///  Event delegate raised when restart fade is completed
        /// </summary>
        public event Action OnRestartFadeCompleted = null;

        /// <summary>
        /// Raised when aniomation is completed 
        /// </summary>
        public void AnimationCompleteEventRaise()
        {
            OnMainGameAnimationComplete?.Invoke();
        }

        /// <summary>
        /// Raised when aniomation uis completed
        /// </summary>
        public void RestartFadeCompleted()
        {
            OnRestartFadeCompleted?.Invoke();
        }

    }
}
